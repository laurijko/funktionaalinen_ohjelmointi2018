/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat;

import java.util.Random;
import static java.util.stream.Collectors.counting;
import java.util.stream.Stream;

/**
 *
 * @author lauri
 */
public class teht3 {
    public static void main(String[] args){
        Random rn = new Random();
        long kutoset = Stream.iterate(Math.abs(rn.nextInt()), n -> Math.abs(rn.nextInt()))
                .limit(20)
                .map(value -> value%6+1)
                .filter(i -> i==6)
                .count();
        System.out.println(kutoset);
    }
}
