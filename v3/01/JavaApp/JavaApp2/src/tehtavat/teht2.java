/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat;

import java.util.ArrayList;
import java.util.Arrays;
import static java.util.Comparator.comparing;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.*;
import streams.Trader;
import streams.Transaction;

import menu.Dish;
import menu.Dish.Type;
import static menu.Dish.menu;

/**
 *
 * @author lauri
 */
public class teht2 {
    public static void main(String ...args){    
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario","Milan");
        Trader alan = new Trader("Alan","Cambridge");
        Trader brian = new Trader("Brian","Cambridge");
		
		List<Transaction> transactions = Arrays.asList(
            new Transaction(brian, 2011, 300), 
            new Transaction(raoul, 2012, 1000),
            new Transaction(raoul, 2011, 400),
            new Transaction(mario, 2012, 710),	
            new Transaction(mario, 2012, 700),
            new Transaction(alan, 2012, 950)
        );	
        
        
        // Query 1: Find all transactions from year 2011 and sort them by value (small to high).
        List<Transaction> tr2011 = transactions.stream()
                                               .filter(transaction -> transaction.getYear() >= 2012 && transaction.getValue() >= 900)
                                               .sorted(comparing(t -> t.getValue()))
                                               .collect(toList());
        System.out.println(tr2011);

        Map<Type, Integer> map3 = Dish.menu.stream()
                .map(dish->dish.getType())
                .reduce(new HashMap<Type, Integer>(),(Map map, Type ruokalaji)->{
                    if(map.containsKey(ruokalaji))
                        map.replace(ruokalaji, (int)map.get(ruokalaji)+1);
                    else
                        map.put(ruokalaji, 1);
                    return map;
                }, (a,b) -> null);
        System.out.println(map3);

    }
}