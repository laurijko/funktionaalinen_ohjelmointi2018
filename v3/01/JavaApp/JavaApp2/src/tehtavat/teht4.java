/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;

/**
 *
 * @author lauri
 */
public class teht4 {
    public static void main(String[] args) {
        List<Integer> lista1 = Arrays.asList(1,2,3);
        List<Integer> lista2 = Arrays.asList(4,5);
        
        //[(1,4),(1,5),(2,4),(2,5),(3,4),(3,5)]
        List<int[]> pairs =
                        lista1.stream()
                                .flatMap((Integer i) -> lista2.stream()
                                                       .map((Integer j) -> new int[]{i, j})
                                 )
                                .collect(toList());
        
         pairs.forEach(pair -> System.out.println("(" + pair[0] + ", " + pair[1] + ")"));

    }
}
