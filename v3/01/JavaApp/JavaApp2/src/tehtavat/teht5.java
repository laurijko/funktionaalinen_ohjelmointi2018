/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;

/**
 *
 * @author lauri
 */
public class teht5 {
    public static void main(String[] args) throws IOException {
    
        String text = readFile("..\\kalevala.txt", Charset.defaultCharset());
        System.out.println(text);
        String s = "This is a test. This is only a test.";
        s = text;
        Map<String, Integer> map = new Hashtable<String, Integer>();
        List<String> list = Arrays.asList(s.split(" ")).stream()
                .map(str->{str=str
                        .toLowerCase()
                        .replace("\n", "")
                        .replace(".","")
                        .replace(",","")
                        .replace(";","")
                        .replace(":","")
                        .replace("?","")
                        .replace("!","");
                        if(map.containsKey(str)){
                            map.replace(str, map.get(str)+1);
                        } else {
                            map.put(str, 1);
                        }
                        return str;
                    })
            .collect(toList());
        
        System.out.println(map);
    }
    
    public static Map<String, Integer> countWords(String text){
        Map<String, Integer> map = new Hashtable<String, Integer>();
        Arrays.asList(text.split(" ")).stream()
                .map(str->{str=str
                        .toLowerCase()
                        .replace("\n", "")
                        .replace(".","")
                        .replace(",","")
                        .replace(";","")
                        .replace(":","")
                        .replace("?","")
                        .replace("!","");
                        if(map.containsKey(str)){
                            map.replace(str, map.get(str)+1);
                        } else {
                            map.put(str, 1);
                        }
                        return str;
                    })
            .collect(toList());
        return map;
    }
    
    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
