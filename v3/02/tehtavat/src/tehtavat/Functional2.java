package tehtavat;

import java2_OmaFIF.*;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

class Functional2{
    
    // Hyödynnetään itse määriteltyä GeneratorFIF-rajapintaa
    // ja Tulostaja-luokkaa


    public static void main(String[] args){
   
        
        // Koska kyseessä funktionaalinen rajapinta, voidaan hyödyntää lambda-lausekkeita:
        
        Supplier generaattori1 = () -> 2;  
        Supplier generaattori2 = () -> (int)(Math.random() * 6 + 1);  
        
        Tulostaja t = new Tulostaja();
        
        t.tulosta(generaattori1);
        t.tulosta(generaattori2);
        t.tulosta(()->100);
        
    }   
    
}


