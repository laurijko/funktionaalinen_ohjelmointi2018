/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;

/**
 *
 * @author lauri
 */
public class lotto {
    public static void main(String[] args) {
        System.out.println("\nLottorivi:");
        int[] lottorivi = arvoLottorivi2().toArray();
        for(int numero : lottorivi)
            System.out.println(numero);
        

        System.out.println("\nLottorivi3:");
        int[] lottorivi2 = arvoLottorivi3().get().toArray();
        for(int numero : lottorivi2)
            System.out.println(numero);
            
    }
    
    public static IntStream arvoLottorivi(){
        List<Integer> numerot = IntStream.range(1, 40).boxed().collect(toList());
        Collections.shuffle(numerot);
        return numerot.stream().mapToInt(i->i).limit(7);
    }
    
    public static IntStream arvoLottorivi2(){
        return IntStream.range(1, 40).boxed().collect(toList()).stream()
                .reduce(new ArrayList<Integer>(), (List l, Integer n)->{
                    l.add(n);
                    Collections.shuffle(l);
                    return l;
                },(a,b) -> null).stream().mapToInt(i->(int)i).limit(7);
    }
    
    public static Supplier<IntStream> arvoLottorivi3(){
        return new Supplier<IntStream>(){
            @Override
            public IntStream get() {
                return arvoLottorivi();
            }
            
        };
    }

}
