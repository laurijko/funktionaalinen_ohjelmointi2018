package tehtavat;

import Tehtavapohjat.*;
import java.util.function.DoubleUnaryOperator;

/*tehtävä 2*/
public class Makihyppy {

    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet){
            return (matka) -> 60+lisapisteet*(matka-kPiste);
    }
        
    public static void main(String[] args) {

       
       DoubleUnaryOperator normaaliLahti = makePistelaskuri(90, 1.8);
       
       System.out.println(normaaliLahti.applyAsDouble(100)); 
          
    }
    
}