/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat;

import java.util.function.IntSupplier;
import java.util.stream.IntStream;

/**
 *
 * @author lauri
 */
public class fibo {
    public static void main(String[] args) {
        
        IntSupplier fibo = new IntSupplier() {
            int n1=0,n2=1,n3;
            @Override
            public int getAsInt() {
                n3=n1+n2;
                n1=n2;
                n2=n3;
                return n3;
            }
        };
        IntStream.generate(fibo).limit(10).forEach(n-> System.out.println(n));
    }
    
}
 