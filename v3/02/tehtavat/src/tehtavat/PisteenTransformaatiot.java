package tehtavat;

import Tehtavapohjat.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;

public class PisteenTransformaatiot {
          
    public static void main(String[] args) {
                    
       Function siirto = Piste.makeSiirto(1, 2);
       Function skaalaus = Piste.makeSkaalaus(2);
       Function kierto = Piste.makeKierto(Math.PI);
       Function muunnos = siirto
               .andThen(skaalaus)
               .andThen(kierto);
       
       Piste[] pisteet = {new Piste(1,1), new Piste(2,2), new Piste(3,3)};
       List<Piste> uudetPisteet = new CopyOnWriteArrayList();
       
       for (Piste p: pisteet){
           uudetPisteet.add((Piste)muunnos.apply(p));
       } 
  
       uudetPisteet.forEach(p -> System.out.println(p));
    }

    private static class Piste {
        int x;
        int y;

        private static Function<Piste, Piste> makeSiirto(int dx, int dy) {
            return (Piste p)->{
                p.x+=dx;
                p.y+=dy;
                return p;
            };
        }

        private static Function<Piste, Piste> makeSkaalaus(int i) {
            return (Piste p)->{
                p.x *= i;
                p.y *= i;
                return p;
            };
        }

        private static Function<Piste, Piste> makeKierto(double PI) {
            return (Piste p)->{
                p.x = (int) (p.x*Math.cos(PI)-p.y*Math.sin(PI));
                p.y = (int) (p.x*Math.sin(PI)+p.y*Math.cos(PI));
                return p;
            };
        }

        public Piste() {
            
        }

        private Piste(int x, int y) {
            this.x=x;
            this.y=y;
        }

        @Override
        public String toString() {
            return "Piste{" + "x=" + x + ", y=" + y + '}';
        }
    }
}