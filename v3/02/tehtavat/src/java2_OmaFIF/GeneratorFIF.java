package java2_OmaFIF;

@FunctionalInterface
public interface GeneratorFIF {
    public abstract int get();
}