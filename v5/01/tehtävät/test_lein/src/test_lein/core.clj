(ns test-lein.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!")
  (println (+ (* 2 5) 4))
  (println (+ 1 2 3 4 5))
  (def tyly (fn [name] (str "Tervetuloa Tylypahkaan " name)))
  (println (tyly "Lauri")) 
  (def mappi {:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}})
  (println ((mappi :name) :middle))
)

(defn square [numero] (* numero numero))

(defn karkausvuosi? [vuosi] 
  (or
    (and (= (rem vuosi 4) 0) (> (rem vuosi 100) 0)); Jaollinen 4:llä ja ei 100:lla 
    ; TAI
    (= (rem vuosi 400) 0); Jaollinen 400:lla
  )
)
