(ns tehtavat.core
  (:gen-class))


(defn parillinen? [numero]
  (= (rem numero 2) 0))

(defn teht1 []
  (print "Anna luku: ")
  (flush)
  (let [line (read-line)
        value (try (Integer/parseInt line)
                   (catch NumberFormatException e line))]
    (println
      (condp instance? value
        Number (if-let [numero (parillinen? value)]
               "parillinen"
               "pariton")
        String "Virheellinen syöte"
      )
    )
    (if (instance? String value) (recur)); Teht2
) )

(defn teht3 [yläraja]
  (loop [numero 1]
    (if (= (rem numero 3) 0)
    (println numero))
    (if (< numero yläraja) (recur (inc numero))) 
) )

(defn teht4 []
  (take 7 (shuffle (range 1 41)))
)

(defn teht5 [p, q]
  (if (= q 0) p 
    (recur q (rem p q))
) )

(defn -main [& args]
  ; (teht1); ja teht2
  ; (teht3 30)
  ; (println (teht4))
  ; (println (teht5 102 68))
)
