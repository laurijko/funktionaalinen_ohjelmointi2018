package testing;


import java.util.*;
import java.util.stream.Collectors;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Debugging{
    public static void main(String[] args) {
        List<Point> points = Arrays.asList(new Point(12, 2), null);
        points.stream().map(p -> p.getX()).forEach(System.out::println);
    }


    private static class Point{

        private static List<Point> moveAllPointsRightBy(List<Point> points, int i) {
            return points.stream()
                    .map(point->point.moveRightBy(i))
                    .collect(Collectors.toList());
        }
        
        private int x;
        private int y;

        private Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }
        
        public Point moveRightBy(int x) {
            return new Point(this.x + x, this.y);
        }
    }
    
    @Test
    public void testmoveAllPointsRightBy() throws Exception {
        List<Point> points = Arrays.asList(new Point(5,5), new  Point(10,5));
        List<Point> expectedPoints = Arrays.asList(new Point(15,5), new Point(20, 5));
        List<Point> newPoints = Point.moveAllPointsRightBy(points, 10);
        assertEquals(expectedPoints, newPoints);
    }
}
