/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 *
 * @author lauri
 */
public class Neliosummat {

    public static long linkedNelioSumma(LinkedList<Long> linkitettyLista) {
        return linkitettyLista.parallelStream()
                                  .map(x -> x * x)
                                  .reduce(0L, (acc, x) -> acc + x);
    }
    
    public static long arrayNelioSumma(ArrayList<Long> taulukkoLista) {
        return taulukkoLista.parallelStream()
                                  .map(x -> x * x)
                                  .reduce(0L, (acc, x) -> acc + x);
    }
    
    public static long streamNelioSumma(int k){
        return LongStream.iterate(1, i->++i).limit(10000000).parallel().reduce(0L, (l,i)->l+i*i);
    }

}
