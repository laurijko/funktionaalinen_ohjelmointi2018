/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 *
 * @author lauri
 */
public class teht3 {
    public static void main(String[] args) {
        System.out.println("Generoidaan sisältö");
        List<Long> list = LongStream.iterate(1, i->++i).limit(10000000).boxed().collect(Collectors.toList());
        System.out.println("Luodaan ArrayList");
        ArrayList<Long> taulukkoLista = new ArrayList(list);
        System.out.println("Luodaan LinkedList");
        LinkedList<Long> linkitettyLista = new LinkedList(list);
        
        
        System.out.println("Linkitetty lista: " + measurePerf(Neliosummat::linkedNelioSumma, linkitettyLista) + " msecs");
        System.out.println("Taulukkolista: " + measurePerf(Neliosummat::arrayNelioSumma, taulukkoLista) + " msecs");
        System.out.println("Stream: " + measurePerf(Neliosummat::streamNelioSumma, 0) + " msecs");
        

    }
    
    public static <T, R> long measurePerf(Function<T, R> f, T input) {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            R result = f.apply(input);
            long duration = (System.nanoTime() - start) / 1_000_000;
            System.out.println("Result: " + result);
            if (duration < fastest) fastest = duration;
        }
        return fastest;
    }
}
