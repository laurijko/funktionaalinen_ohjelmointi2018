/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat;

import java.util.stream.IntStream;

/**
 *
 * @author lauri
 */
public class teht2 {
    static int filter=0;
    static int map=0;
    public static void main(String[] args) {
        //  sum of the triples of even integers from 2 to 10
        
        System.out.println("filter->map");
        System.out.printf(
        "Sum of the triples of even integers from 2 to 10 is: %d%n",
        IntStream.rangeClosed(1,10)
                .filter(x -> {
                    filter++;
                    System.out.println("filter: "+filter);
                    return x%2==0;})
                .map(x -> {
                    map++;
                    System.out.println("map: "+map);
                    return x*3;})
                .sum());
        
        System.out.println("\nmap->filter");
        filter = map = 0;
        System.out.printf(
        "Sum of the triples of even integers from 2 to 10 is: %d%n",
        IntStream.rangeClosed(1,10)
                .map(x -> {
                    map++;
                    System.out.println("map: "+map);
                    return x*3;})
                .filter(x -> {
                    filter++;
                    System.out.println("filter: "+filter);
                    return x%2==0;})
                .sum());
    }
   
}
