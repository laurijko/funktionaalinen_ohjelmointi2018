/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtävät;

import static java.lang.Thread.sleep;
import java.util.Observable;

/**
 *
 * @author lauri
 */
abstract class Obsrnble extends Observable implements Runnable {
    @Override
    public abstract void run();
}

public class teht8 {

    public static void main(String[] args) {
        Obsrnble uutistoimisto = new Obsrnble(){
            @Override
            public void run() {
                String[] uutiset = new String[] {
                    "politiikka", "urheilu", "julkkis", "arvonta"
                };
                for(String uutinen : uutiset){
                    try {
                        setChanged();
                        notifyObservers(uutinen);
                        sleep(100);
                    } catch (InterruptedException ex) {
                    }
                }
            }
        };
        
        uutistoimisto.addObserver((Observable o, Object arg) -> {
            if( ((String)arg).matches("politiikka") )
                System.out.println("Helsingin Sanomat: " +arg);
        });
        
        uutistoimisto.addObserver((Observable o, Object arg) -> {
            if( ((String)arg).matches("julkkis") )
                System.out.println("Ilta-Sanomat: " +arg);
        });
        
        Thread t1 = new Thread(uutistoimisto);
        t1.start();
    }
    
}
