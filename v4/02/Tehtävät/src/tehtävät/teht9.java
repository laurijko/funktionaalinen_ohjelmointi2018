
package tehtävät;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.Random;

/**
 *
 * @author lauri
 */
public class teht9 {
    
    static class RandomPeli {
        
        Random rand;
        int vuoro;
        int[] pisteet;
        
        
        public RandomPeli(){
            pisteet = new int[2];
            vuoro = 1;
            rand = new Random();
        }
        
        public void heitä(){
            if(rand.nextInt(100)+1 >50)
                pisteet[vuoro%2]++;
            vuoro++;
        }
        
        public boolean voittaja(){
            if(vuoro<7)
                return false;
            else
                return true;
        }
        
        public void tulostaVoittaja(){
            if(pisteet[0]==pisteet[1])
                System.out.println("Tasapeli!");
            else if(pisteet[0] > pisteet[1]){
                System.out.println("Pelaaja 1 voitti!");
            } else {
                System.out.println("Pelaaja 2 voitti");
            }
            System.out.println("Pelaaja 1: "+ pisteet[0]+"\nPelaaja 2: "+pisteet[1]);
        }
        
    }
    
    static class Game<T>{
        
        public final void playOneGame(
                Supplier<T> initializeGame,
                Predicate<T> endOfGame,
                Consumer<T> makePlay,
                Consumer<T> printWinner)
        {
            T peli = initializeGame.get();
            while (!endOfGame.test(peli)){
                makePlay.accept(peli);
            }
            printWinner.accept(peli);
        }
        
    }
    
    public static void main(String[] args) {
        
        Game<RandomPeli> peli = new Game<>();
        
        peli.playOneGame(
                ()-> new RandomPeli(),  // initializeGame
                (p)->p.voittaja(),      // endOfGame
                (p)->p.heitä(),         // makePlay
                (p)->p.tulostaVoittaja()// printWinner
        );
        
    }

}
