/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtävät;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lauri
 */


public class teht7 {
    
    public static void main(String[] args) {
        
        StrategyContext muokattava = new StrategyContext();
        muokattava.setData("ÄäÅåÖö          sturct      sturct      ");
        
        muokattava.addStrategy((String text) -> text
                .replaceAll(" {2,}", " "));
         
        muokattava.addStrategy((text) -> text
                .replaceAll("[äå]", "a")
                .replaceAll("[ÄÅ]", "A")
                .replaceAll("[ö]", "o")
                .replaceAll("[Ö]", "O"));
        
        muokattava.addStrategy((text) -> text
                .replaceAll("sturct", "struct"));
        
        System.out.println(muokattava.getData());
         
    }
}
    
@FunctionalInterface
interface StrategyIF{
     String apply(String s);
}

class StrategyContext {
        
    private List<StrategyIF> strategiat;

    private String data;
    
    public StrategyContext() {
        strategiat = new ArrayList<>();
    }

    public String getData() {
        String output = data;
        for(StrategyIF s : strategiat)
            output = s.apply(output);
        return output;
    }

    public void setData(String data) {
        this.data = data;
    }
    
    public void addStrategy(StrategyIF strategy){
        strategiat.add(strategy);
    }
    
    public void clearStrategies(){
        strategiat.clear();
    }

}



