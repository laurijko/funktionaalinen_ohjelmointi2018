/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtävät;

import java.util.function.Function;
import java.util.function.UnaryOperator;
/**
 *
 * @author lauri
 */
public class teht6 {
    
    
    
    
    public static void main(String[] args) {
        
        UnaryOperator<String> poistaYlimääräisetVälilyönnit = (text) -> text
                .replaceAll(" {2,}", " ");
        
        UnaryOperator<String> korvaaSkandit = (text) -> text
                .replaceAll("[äå]", "a")
                .replaceAll("[ÄÅ]", "A")
                .replaceAll("[ö]", "o")
                .replaceAll("[Ö]", "O");
        
        UnaryOperator<String> tarkistaOikeinkirjoitus = (text) -> text
                .replaceAll("sturct", "struct");
        
        Function<String, String> ketju = poistaYlimääräisetVälilyönnit.andThen(korvaaSkandit).andThen(tarkistaOikeinkirjoitus);
        
        String texti = "ÄäÅåÖö          sturct      sturct      ";
        
        texti = ketju.apply(texti);
        
        System.out.println(texti);
    }
}
