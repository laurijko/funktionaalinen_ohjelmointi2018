/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtävät;

import esimerkit.Omena;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.*;
import static java.util.stream.Collector.Characteristics.*;


/**
 *
 * @author lauri
 */



public class teht1 {
    static class OmatKollektorit {
            
        public static Collector<Object, StringBuffer, String> JoinCollector(Boolean concurrent){
            return new Collector<Object, StringBuffer, String>(){
                @Override
                public Supplier<StringBuffer> supplier() {
                    return () -> new StringBuffer();
                }

                @Override
                public BiConsumer<StringBuffer, Object> accumulator() {
                    return (stringBuffer, object) -> stringBuffer
                            .append(object)
                            .append("\n");
                }

                @Override
                public BinaryOperator<StringBuffer> combiner() {
                    return (sb1, sb2) -> sb1.append(sb2);
                }

                @Override
                public Function<StringBuffer, String> finisher() { 
                    return (stringBuffer)->stringBuffer.toString();
                }

                @Override
                public Set<Collector.Characteristics> characteristics() {
                    if(concurrent)
                        return Collections.unmodifiableSet(EnumSet.of(CONCURRENT, UNORDERED));
                    return Collections.unmodifiableSet(EnumSet.of(UNORDERED));
                }
            };
        }
    }

    public static void main(String[] args) {
        long size = 100000L;
        
        Function concurrent = (t)->Stream
                .generate(()-> new Omena("vihreä", 3))
                .limit(size)
                .collect(OmatKollektorit.JoinCollector((boolean)t));
        
        Function concurrentParallel = (t)->Stream
                .generate(()-> new Omena("vihreä", 3))
                .parallel()
                .limit(size)
                .collect(OmatKollektorit.JoinCollector((boolean)t));
        
        Function parallel = (t)->Stream
                .generate(()-> new Omena("vihreä", 3))
                .parallel()
                .limit(size)
                .collect(OmatKollektorit.JoinCollector((boolean)t));
        
        Function nothing = (t)->Stream
                .generate(()-> new Omena("vihreä", 3))
                .limit(size)
                .collect(OmatKollektorit.JoinCollector((boolean)t));
        
        System.out.println("Concurrent + parallel: " + measurePerf(concurrentParallel, true) + " msecs");
        System.out.println("Concurrent: " + measurePerf(concurrent, true) + " msecs");
        System.out.println("Parallel: " + measurePerf(parallel, false) + " msecs");
        System.out.println("Nothing: " + measurePerf(nothing, false) + " msecs");


    }
    
    public static <T, R> long measurePerf(Function<T, R> f, T input) {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            R result = f.apply(input);
            long duration = (System.nanoTime() - start) / 1_000_000;
            //System.out.println("Result: " + result);
            if (duration < fastest) fastest = duration;
        }
        return fastest;
    }
}
