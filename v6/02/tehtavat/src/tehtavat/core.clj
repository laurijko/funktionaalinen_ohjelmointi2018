(ns tehtavat.core
  (:gen-class))

(defn allekirjoitus [tervehdys nimi]
  (str tervehdys " " nimi)
)

(def teht1 (partial allekirjoitus "Tervesin"))

(def vektori [[1 2 3] [4 5 6] [7 8 9]])

(defn teht2 [v]
  (map #(apply min %) v)
)
(defn teht2b [& v]
  (apply vec v)
)

;teht3
(def vampire-database {
    0 {:makes-blood-puns? false :has-pulse? true :name "McFishwich"}                   
    1 {:makes-blood-puns? false :has-pulse? true :name "McMackson"}                   
    2 {:makes-blood-puns? true :has-pulse? false :name "Damon Salvatore"}                   
    3 {:makes-blood-puns? true :has-pulse? true :name "Mickey Mouse"}                   
})

(defn lisaa-vampyyrikantaan [db mbp hp nimi]
  (assoc db (inc (last (keys db))) {:makes-blood-puns? mbp :has-pulse? hp :name nimi})
)

;teht4
(defn poista-vampyyrikannasta [db i]
  (dissoc db i)
)

(def file "sima.csv") 
 
(def aineet [:aines :yksikko :maara])

(defn str->int
  [str]
  (Integer. str))

(def konversiot {:aines identity
                 :yksikko identity
                 :maara str->int})

(defn konvertoi 
  [avain arvo]
  ((get konversiot avain) arvo))



(defn parse 
   ; "parse tuottaa merkkijonovektoreita:  ["Vesi" "litraa" "10"] jne."
  [string]
  (map #(clojure.string/split % #",")
       (clojure.string/split string #"\n")))

     
(defn tee-mappeja 
  "tuottaa merkkijonovektoreista mappeja seq-peräkkäisrakenteeksi"
  [merkkijonovektoreista]
  (map (fn [merkkijonovektori]  ; anonyymifunktio on mapin 1. arg
            (reduce (fn [uusi-map [avain arvo]]  ; uusi-map on alussa tyhjä map
               (assoc uusi-map avain (konvertoi avain arvo)) ; (assoc map key val) 
            )
            {}  ; reducen 2. arg alussa tyhjä map
            (map vector aineet merkkijonovektori)) ; reducen 3. arg tuottaa seq:n avain-arvo -vektoreita
        )
        merkkijonovektoreista) ; funktion arg = map-funktion 2. arg (luettu tiedostosta)
)

(defn teht5 [kerroin & ainekset]
  (map #(update % :maara * kerroin) (vec ainekset))
)

(defn -main [& args]
  ; (println (teht1 "lauri"))
  ; (println (teht2 vektori))
  ; (println (teht2b (teht2 vektori)))
  ; (println vampire-database)
  ; (println (lisaa-vampyyrikantaan vampire-database true true "vampyyri"))
  ; (println (poista-vampyyrikannasta vampire-database 1))
  (def sima-ainekset (tee-mappeja (parse (slurp file))))
  (println (apply teht5 5 sima-ainekset))
)
