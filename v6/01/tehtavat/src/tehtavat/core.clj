(ns tehtavat.core
  (:gen-class))

(defn average [& params] 
    (/ (reduce + params) (count params)))

(defn teht1 [v1 v2]
  (apply average
    (filter pos?
      ; (map (fn [a,b] (average [a, b])) v1 v2)
      (map average v1 v2)
) ) )

(def food-journal
  [{:kk 3 :paiva 1  :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2  :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5  :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])

(defn teht2 [kk]
  (reduce #(+ %1 (- (%2 :neste) (%2 :vesi))) 0
         (filter #(= kk (% :kk)) food-journal)) 
)

(defn teht3 [kk]
  (map #(sorted-map
         :kk (% :kk) 
         :paiva (% :paiva) 
         :muuneste (- (% :neste) (% :vesi)))
      (filter #(= kk (% :kk)) food-journal))
)

(defn -main []
  ; (println (teht1 [1 2 3] [-2 -2 -2]))
  ; (println (teht2 4))
  (println (teht3 4))
)
