const v2016 = [1, 2, -3, -4, -3, 0, 2, 4, 5, -2, 0, 1]
const v2015 = [2, -1, 1, 2, -3, 1, -1, 6, 8, -5, -5, -3]



var keskarit = v2016.map((arvo, index) => (v2015[index]+arvo)/2)
	.filter(arvo => arvo>=0)
	.reduce((acc, arvo) => (acc + arvo/12))

console.log(keskarit)
