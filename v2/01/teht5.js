
var text = "this is a test. This is only a test."
console.log(text.toLowerCase().match(/\w+/g).reduce((a,w) => { (a[w]) ? a[w]++ : a[w]=1; return a },{}))

var fs = require('fs') , filename = process.argv[2];
fs.readFile(filename, 'utf8', function(err, data) {
	if (err) throw err;
	console.log('OK: ' + filename);
	console.log(data.toLowerCase().match(/[A-Za-z0-9ÄäÖöÅå\-\']+/g).reduce((a,w) => { (a[w]) ? a[w]++ : a[w]=1; return a },{}))
});

