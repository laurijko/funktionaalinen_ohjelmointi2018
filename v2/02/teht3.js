const Immutable = require('immutable')

const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);
const set2 = set1 & 'ruskea'
console.log("set1 === set2 :"+ (set1 === set2))
const set3 = set2 & 'ruskea'
console.log("set2 === set3 :"+ (set2 === set3))

const Auto = function(){
	let tankki = 100;
	let matkamittari = 0;
	return {
		aja: function(matka){ 
			if(tankki >= matka) {
				matkamittari += matka
				tankki -= matka
				return matka
			}
			return 0
		},
		tankkaa: function(maara){
			tankki+maara>100 ? tankki = 100 : tankki += maara
		},
		getTankki: function(){return tankki},
		getMatkamittari: function(){return matkamittari}
	}
}

const lada = Auto()
const volvo = Auto()

console.log("Ladan tankki on " + lada.getTankki()+"/100 ja ajettu " + lada.getMatkamittari()+" metriä")
console.log("Ajetaan ladalla "+lada.aja(30) + " metriä")
console.log("Ladan tankki on " + lada.getTankki()+"/100 ja ajettu " + lada.getMatkamittari()+" metriä")

console.log("\nVolvon tankki on " + volvo.getTankki()+"/100 ja ajettu " + volvo.getMatkamittari()+" metriä")
console.log("Ajetaan volvolla "+volvo.aja(43) + " metriä")
console.log("Ajetaan volvolla "+volvo.aja(21) + " metriä")
console.log("Volvon tankki on " + volvo.getTankki()+"/100 ja ajettu " + volvo.getMatkamittari()+" metriä")

console.log("\nTankataan ladaa +20")
lada.tankkaa(20)
console.log("Ladan tankki on "+lada.getTankki()+"/100")
