const Auto = (function(){
		const suojatut = new WeakMap()

		class Auto {
				constructor(){
						suojatut.set(this, {tankki: 100, matkamittari: 0})
				}
				aja(matka){ 
						if(suojatut.get(this).tankki >= matka) {
								suojatut.get(this).matkamittari += matka
								suojatut.get(this).tankki -= matka
								return matka
						}
						return 0
				}
				tankkaa(maara){
						suojatut.get(this).tankki+maara>100 ? suojatut.get(this).tankki = 100 : suojatut.get(this).tankki += maara
				}
				getTankki(){return suojatut.get(this).tankki}
				getMatkamittari(){return suojatut.get(this).matkamittari}
		}
		return Auto
})()

const lada = new Auto()
const volvo = new Auto()

console.log("Ladan tankki on " + lada.getTankki()+"/100 ja ajettu " + lada.getMatkamittari()+" metriä")
console.log("Ajetaan ladalla "+lada.aja(30) + " metriä")
console.log("Ladan tankki on " + lada.getTankki()+"/100 ja ajettu " + lada.getMatkamittari()+" metriä")

console.log("\nVolvon tankki on " + volvo.getTankki()+"/100 ja ajettu " + volvo.getMatkamittari()+" metriä")
console.log("Ajetaan volvolla "+volvo.aja(43) + " metriä")
console.log("Ajetaan volvolla "+volvo.aja(21) + " metriä")
console.log("Volvon tankki on " + volvo.getTankki()+"/100 ja ajettu " + volvo.getMatkamittari()+" metriä")

console.log("\nTankataan ladaa +20")
lada.tankkaa(20)
console.log("Ladan tankki on "+lada.getTankki()+"/100")
