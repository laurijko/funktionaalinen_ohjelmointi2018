const Immutable = require('immutable');

let tulos = Immutable.Range(1, Infinity)
  .skip(10000)
  .map(n => {console.log(n);return n})
  .filter(n => n % 5 === 0)
  .take(2)
  
console.log(tulos);

