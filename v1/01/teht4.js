/*
Kirjoita potenssiin korotus rekursiivisena funktiona.
*/

function potenssi(luku, p){
    if(p === 0){
        return 1;
    } else {
        return luku*potenssi(luku, p-1);
    }
}

console.log(process.argv[2]+"^"+process.argv[3]);
console.log(potenssi(process.argv[2], process.argv[3]));