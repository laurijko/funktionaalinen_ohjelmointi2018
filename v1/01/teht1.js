/*
onPalindromi(merkkijono) {
  Jos merkkijonon pituus on 0 tai 1, palauta true.
  Muuten jos merkkijonon ensimmäinen ja viimeinen merkki ovat erilaiset, palauta false.
  Muissa tapauksissa ota jonon keskiosa, josta puuttuvat ensimmäinen ja viimeinen merkki,
    selvitä rekursiivisella metodikutsulla, onko keskiosa palindromi, ja
    palauta sama totuusarvo, jonka rekursiivinen kutsukin palautti.
}
*/

function onPalindromi(merkkijono){
    console.log(merkkijono);
    if(merkkijono.length <= 1){
        return true;
    } else if(merkkijono[0] != merkkijono[merkkijono.length-1]) {
        return false
    } else {
        return onPalindromi(merkkijono.slice(1, merkkijono.length-1));
    }
}

console.log(onPalindromi(process.argv[2]));