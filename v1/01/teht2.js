/*
syt(p, q) {
  Jos q on 0, palauta p.
  Muuten 
    selvitä rekursiivisella metodikutsulla, mikä on suurin yhteinen tekijä q:lle ja p%q:lle,
    ja
    palauta sama luku, jonka rekursiivinen kutsukin palautti.
}
*/

function syt(p, q){
    if(q === 0){
        return p;
    } else {
        return syt(q, p%q);
    }
}

/*
Keskenään jaottomiksi tai suhteellisiksi alkuluvuiksi tai alkuluvuiksi toistensa
suhteen sanotaan kahta lukua p ja q, jos p:n ja q:n suurin yhteinen tekijä on 1.
Sovella edellisen tehtävän algoritmia ja tee rekursiivinen funktio kjl(p, q),
joka tutkii ovatko kaksi lukua keskenään jaottomia. Esimerkiksi 35 ja 18 ovat
keskenään jaottomia lukuja.
*/

function kjl(p, q){
    if(syt(p,q) === 1){
        return true
    } else {
        return false
    }
}

console.log("suurin yhteinentekijä:");
console.log(syt(process.argv[2], process.argv[3]));
console.log("Ovatko jaottomia: ");
console.log(kjl(process.argv[2], process.argv[3]));