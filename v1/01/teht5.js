function käännä(lista){
    let eka = lista[0];
    let vika = lista[lista.length-1]
    
    if(lista.length <= 3) {
        lista[lista.length-1] = eka;
        lista[0] = vika;
        return lista;
    } else {
        let lista2 = [];
        lista2.push(vika);
        lista2 = lista2.concat(käännä(lista.slice(1, lista.length-1)))
        lista2.push(eka);
        return lista2;
    }
}

function reverse(w){
    return (function r(u, w) {
        if (w.length < 1) return u
        u.push(w.pop())
        return r(u, w)
    })([], w)
}

var lista = process.argv.slice(2, process.argv.length);
console.log(lista);
console.log("")
console.log(reverse(lista));