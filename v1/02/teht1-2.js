function vertaa() {
	return function(a, b){
		if(a > b) {
			return 1
		} else if(b > a) {
			return -1
		} else {
			return 0
		}
	}
}

function vertaaTauluja(funk, taulu1, taulu2){
	let t1 = taulu1
	let t2 = taulu2
	if(t1.length && t2.length) {
		return funk(t1.pop(), t2.pop()) + vertaaTauluja(funk, t1, t2)
	}
	return 0;
}

v15 = [12, 14, 11, 15, 16, 17]
v16 = [11, 13, 13, 15, 17, 18]

console.log(vertaaTauluja(vertaa(), v15, v16))
