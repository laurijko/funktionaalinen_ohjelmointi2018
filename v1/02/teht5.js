var Moduuli = (function() {
	let x = 0
	let kasvata = () => ++x
	let vahenna = () => --x
	return {kasvata, vahenna}
})()
console.log(Moduuli.kasvata())
console.log(Moduuli.kasvata())
console.log(Moduuli.vahenna())
console.log(Moduuli.vahenna())
